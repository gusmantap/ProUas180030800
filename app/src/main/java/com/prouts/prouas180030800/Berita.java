package com.prouts.prouas180030800;

public class Berita {
    private String id;
    private String image;
    private String judul;
    private String isi;
    private String tanggal;
    private String sumber;


    public Berita(String id, String image, String judul, String isi, String tanggal, String sumber) {
        this.id = id;
        this.image = image;
        this.judul = judul;
        this.isi = isi;
        this.tanggal = tanggal;
        this.sumber = sumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getSumber() {
        return sumber;
    }

    public void setSumber(String sumber) {
        this.sumber = sumber;
    }
}

