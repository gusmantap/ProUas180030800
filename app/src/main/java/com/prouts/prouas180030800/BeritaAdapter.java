package com.prouts.prouas180030800;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class BeritaAdapter extends RecyclerView.Adapter<BeritaAdapter.ViewHolder> {

    private ArrayList<Berita> rvData;

    public BeritaAdapter(ArrayList<Berita> inputData) {
        rvData = inputData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_berita, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String id = rvData.get(position).id;
        holder.tvTitle.setText(rvData.get(position).judul);
        holder.tvIsi.setText(rvData.get(position).isi);
        holder.tvTanggal.setText(rvData.get(position).tanggal);
        holder.tvSumber.setText(rvData.get(position).sumber);
        Picasso.get().load(rvData.get(position).image).into(holder.gambar);
    }

    @Override
    public int getItemCount() {
        return rvData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle;
        public TextView tvTanggal;
        public TextView tvIsi;
        public ImageView gambar;
        public TextView tvSumber;

        public ViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.tv_judul);
            tvTanggal = v.findViewById(R.id.tv_tanggal);
            tvIsi = v.findViewById(R.id.tv_isi);
            tvSumber = v.findViewById(R.id.tv_sumber);
            gambar = v.findViewById(R.id.gambar);
        }
    }
}