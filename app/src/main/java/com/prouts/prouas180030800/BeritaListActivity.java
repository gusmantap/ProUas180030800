package com.prouts.prouas180030800;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.prouts.prouas180030800.dummy.DummyContent;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * An activity representing a list of beritas. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link BeritaDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class BeritaListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        Add backbutton
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setTitle(getTitle());

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if (findViewById(R.id.berita_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        View recyclerView = findViewById(R.id.berita_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        // Setup tampilan supaya bisa grid layout. 2 rows
        GridLayoutManager layoutManager = new GridLayoutManager(this ,2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new BeritaAdapter(this, DummyContent.ITEMS, mTwoPane));
    }

    public static class BeritaAdapter
            extends RecyclerView.Adapter<BeritaAdapter.ViewHolder> {

        private final BeritaListActivity mParentActivity;
        private final List<Berita> mValues;
        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Berita item = (Berita) view.getTag();
                Log.e("onclick", ""+item);
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(BeritaDetailFragment.ARG_ITEM_ID, item.id);
                    BeritaDetailFragment fragment = new BeritaDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.berita_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, BeritaDetailActivity.class);
                    intent.putExtra(BeritaDetailFragment.ARG_ITEM_ID, item.id);

                    context.startActivity(intent);
                }
            }
        };

        BeritaAdapter(BeritaListActivity parent,
                      List<Berita> items,
                      boolean twoPane) {
            mValues = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_berita, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.tvTitle.setText(mValues.get(position).judul);
            holder.tvIsi.setText(mValues.get(position).isi.substring(0, 50)+"..."); // Excerpt text
            holder.tvTanggal.setText(mValues.get(position).tanggal);
            holder.tvSumber.setText(mValues.get(position).sumber);
            Picasso.get().load(mValues.get(position).image).into(holder.gambar);

            holder.itemView.setTag(mValues.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public TextView tvTitle;
            public TextView tvTanggal;
            public TextView tvIsi;
            public ImageView gambar;
            public TextView tvSumber;

            ViewHolder(View v) {
                super(v);
                tvTitle = v.findViewById(R.id.tv_judul);
                tvTanggal = v.findViewById(R.id.tv_tanggal);
                tvIsi = v.findViewById(R.id.tv_isi);
                tvSumber = v.findViewById(R.id.tv_sumber);
                gambar = v.findViewById(R.id.gambar);
            }
        }
    }
}
