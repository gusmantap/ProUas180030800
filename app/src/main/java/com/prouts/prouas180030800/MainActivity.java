package com.prouts.prouas180030800;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnBiodata = findViewById(R.id.btnBiodata);
        btnBiodata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent biodata = new Intent(MainActivity.this, BiodataActivity.class);
                startActivity(biodata);
            }
        });
        Button btnBerita = findViewById(R.id.btnBerita);
        btnBerita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent berita = new Intent(MainActivity.this, BeritaListActivity.class);
                startActivity(berita);
            }
        });
    }
}
