package com.prouts.prouas180030800.dummy;

import com.prouts.prouas180030800.Berita;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final ArrayList<Berita> ITEMS = new ArrayList<Berita>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, Berita> ITEM_MAP = new HashMap<String, Berita>();

    static {
        addItem(new Berita("1", "https://akcdn.detik.net.id/visual/2020/03/19/1cdabc18-2c80-4c05-a81c-94a1443a6789_169.jpeg?w=650", "Update Corona 20 April: 6.760 Positif, 747 Sembuh", "Jumlah pasien positif terinfeksi virus corona (Covid-19) di Indonesia hingga Senin (20/4) berjumlah 6.760 kasus. Sebanyak 590 di antaranya meninggal dunia dan 747 orang dinyatakan sembuh.", "Senin, 20/04/2020 15:45 WIB", "CNN Indonesia"));
        addItem(new Berita("2", "https://akcdn.detik.net.id/community/media/visual/2017/05/29/79a5c917-e4da-48b5-8528-0e39924ba89b_169.jpg?w=700&q=90", "Petani-petani di Poso Hilang, Ditemukan dalam Kondisi Nyawa Melayang", "Dua petani di Poso, Sulawesi Tengah (Sulteng), yang dilaporkan hilang telah ditemukan dengan kondisi nyawa melayang. Peristiwa tersebut masih menjadi misteri karena kedua petani awalnya dilaporkan diculik orang tak dikenal.", "Senin, 20 Apr 2020 22:03 WIB", "Detik.com"));
        addItem(new Berita("3", "https://akcdn.detik.net.id/community/media/visual/2016/03/08/b6443d6c-2855-4e8b-88ca-d851d1340ac4_169.jpg?w=700&q=90", "Viral Pencuri Susu di Minimarket Dikejar Warga hingga Kecelakaan di Cibubur", "Sebuah video memperlihatkan dua orang pria babak belur dalam sebuah mobil, viral di media sosial. Usut punya usut, kedua pelaku ternyata mengalami kecelakaan setelah dikejar warga usai melakukan pencurian di sebuah minimarket.", "Senin, 20 Apr 2020 22:01 WIB", "Detik.com"));
        addItem(new Berita("4", "https://akcdn.detik.net.id/visual/2018/04/12/a63cefa8-d50f-4705-9fed-e47ece9cafad_169.jpeg?w=715&q=90", "Kalau Ada yang Nagih Pakai Debt Collector, OJK: Laporkan!", "Otoritas Jasa Keuangan (OJK) menyebutkan konsumen bisa melaporkan jika masih ada perusahaan pembiayaan (multifinance) yang melakukan penagihan nasabah menggunakan debt collector (penagih utang) di tengah pandemi Covid-19 saat ini. Hal itu mengingat OJK sudah memberikan keringanan kepada debitur untuk bisa melakukan restrukturisasi kredit.", "20 April 2020 15:40", "CNBC Indonesia"));
        addItem(new Berita("5", "https://akcdn.detik.net.id/visual/2019/01/23/e7a98341-6361-435e-a75e-53157058f049_169.jpeg?w=715&q=90", "Penguatan Belum Terbendung, Rupiah Terbaik di Asia Lagi", "Nilai tukar rupiah kembali menguat melawan dolar Amerika Serikat (AS) pada perdagangan Senin (20/4/2020), padahal di awal perdagangan rupiah sempat melemah cukup tajam. Kabar bagus dari China membuat rupiah bangkit dan kembali menguat.", "20 April 2020 17:03", "CNBC Indonesia"));
    }

    private static void addItem(Berita item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }
}
